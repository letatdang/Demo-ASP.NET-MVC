﻿using DemoWeb1.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DemoWeb1.Models
{
    public class Student : User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Student()
        {
            this.Teachers = new HashSet<Teacher>();
            TeacherListId = new List<int>();
        }

        [Key]
        public int StudentId { get; set; }

        public Nullable<int> CourseId { get; set; }

        public virtual Course course { get; set; }

        public List<int> TeacherListId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Teacher> Teachers { get; set; }

        public void Update(Student student, StudentMgntEntities studentMgntEntities)
        {
            // changing content
            this.Name = student.Name;
            this.CourseId = student.CourseId;
            this.course = studentMgntEntities.Courses.Find(student.CourseId);
            this.TeacherListId = student.TeacherListId;
            this.ConfirmPassword = this.Password;
            foreach (int ID in student.TeacherListId)
                this.Teachers.Add(studentMgntEntities.Teachers.Find(ID));
        }
    }
}