﻿using DemoWeb1.Utils.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoWeb1.Models
{
    public class Course
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Course()
        {
            this.Students = new HashSet<Student>();
        }

        [Key]
        public int CourseId { get; set; }

        [Required]
        [StringLength(RepoConstants.NAME_SIZE, ErrorMessage = RepoConstants.NAME_SIZE_ERROR)]
        [Column(RepoConstants.NAME_COLUMN)]
        public String Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Student> Students { get; set; }
    }
}