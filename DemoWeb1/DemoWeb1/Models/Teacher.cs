﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DemoWeb1.Models
{
    public class Teacher : User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Teacher()
        {
            this.Students = new HashSet<Student>();
        }

        [Key]
        public int TeacherId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Student> Students { get; set; }
    }
}