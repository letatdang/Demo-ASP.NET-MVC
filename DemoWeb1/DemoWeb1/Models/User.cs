﻿using DemoWeb1.Utils.Constants;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace DemoWeb1.Models
{
    public class User
    {
        [Required]
        [StringLength(RepoConstants.NAME_SIZE, ErrorMessage = RepoConstants.NAME_SIZE_ERROR)]
        [Column(RepoConstants.NAME_COLUMN)]
        public String Name { get; set; }

        [Required]
        [Display(Name = RepoConstants.USERNAME_COLUMN)]
        [Index(RepoConstants.INDEX_FIRST_SECOND, 1, IsUnique = true)]
        [StringLength(RepoConstants.USERNAME_SIZE)]
        [EmailAddress]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = RepoConstants.PASSWORD_COLUMN)]
        public string Password { get; set; }

        [NotMapped]
        [DataType(DataType.Password)]
        [Compare(RepoConstants.PASSWORD_COLUMN, ErrorMessage = RepoConstants.CONFIRMPASSWORD_MSG)]
        public string ConfirmPassword { get; set; }

        [DisplayName(RepoConstants.IMG_DISPLAY_NAME)]
        [Column(RepoConstants.IMG_COLUMN)]
        public String ImagePath { get; set; }

        [NotMapped]
        public HttpPostedFileBase ImageFile { get; set; }

        public void EncodePassword()
        {
            this.Password = this.ConfirmPassword = Utils.GeneralUtils.Encode(this.Password);
        }

        public void ChangeImage(User user)
        {
            this.ImagePath = user.ImagePath;
            this.ImageFile = user.ImageFile;
        }
    }
}