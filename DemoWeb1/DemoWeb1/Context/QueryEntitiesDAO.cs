﻿using DemoWeb1.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DemoWeb1.Context
{
    public interface IQueryEntitiesDAO
    {
        Student FindStudentByUserName(String _username);
        Teacher FindTeacherByUserName(String _username);

        List<Student> FindStudentByString(String SearchString);
        List<Teacher> FindTeacherByString(String SearchString);

        Student isValidStudent(String _username, String _password);
        Teacher isValidTeacher(String _username, String _password);

        List<String> SearchAutocomplete(String SrchString);
    }

    public class QueryEntitiesDAO : IQueryEntitiesDAO
    {
        private StudentMgntEntities db;

        public QueryEntitiesDAO(StudentMgntEntities dbMgnt)
        {
            db = dbMgnt;
        }

        public Student FindStudentByUserName(String _username)
        {
            return this.db.Students.Where(std => std.UserName.Equals(_username)).SingleOrDefault();
        }

        public Teacher FindTeacherByUserName(String _username)
        {
            return this.db.Teachers.Where(teacher => teacher.UserName.Equals(_username)).SingleOrDefault();
        }

        public Student isValidStudent(String _username, String _password)
        {
            return this.db.Students.Where(std => std.UserName.Equals(_username) && std.Password.Equals(_password)).SingleOrDefault();
        }

        public Teacher isValidTeacher(String _username, String _password)
        {
            return this.db.Teachers.Where(teacher => teacher.UserName.Equals(_username) && teacher.Password.Equals(_password)).SingleOrDefault();
        }

        public Object isValidAccount(String _username, String _password)
        {
            String encoded = Utils.GeneralUtils.Encode(_password);
            Teacher teacher = this.isValidTeacher(_username, encoded);

            if (teacher == null)
                return this.isValidStudent(_username, encoded);
            else
                return teacher;
        }

        public List<Student> FindStudentByString(string SearchString)
        {
            return this.db.Students.Where(std => std.Name.Contains(SearchString) || std.UserName.Contains(SearchString)).ToList();
        }

        public List<Teacher> FindTeacherByString(string SearchString)
        {
            return this.db.Teachers.Where(t => t.Name.Contains(SearchString) || t.UserName.Contains(SearchString)).ToList();
        }

        public List<String> SearchAutocomplete(String SrchString)
        {
            return db.Students.Where(std => std.Name.ToLower().StartsWith(SrchString.ToLower()) || std.Name.Contains(SrchString)).Select(std => std.Name).ToList();
        }
    }
}