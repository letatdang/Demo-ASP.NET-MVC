﻿using DemoWeb1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace DemoWeb1.Context
{
    public interface IStudentDAO
    {
        void Create(Student student);
        Student Delete(int id);
        void Update(Student student);
    }

    public class StudentDAO : Controller, IStudentDAO
    {
        private StudentMgntEntities db;

        public StudentDAO(StudentMgntEntities dbMgnt)
        {
            db = dbMgnt;
        }
        
        public void Create(Student student)
        {
            // move picture to project place (folder Image)
            if (student.ImageFile != null)
                student.ImagePath = Utils.GeneralUtils.UploadToServer(student.ImageFile);

            // encoding password
            student.EncodePassword();

            // adding teacher list
            foreach (int id in student.TeacherListId)
               student.Teachers.Add(db.Teachers.Find(id));

            db.Students.Add(student);
            ModelState.Clear();
            db.SaveChanges();
        }

        public Student Delete(int id)
        {
            Student std = db.Students.Find(id);

            this.db.Students.Remove(std);
            db.Entry(std).State = System.Data.Entity.EntityState.Deleted;
            Utils.GeneralUtils.DeleteFileAtServer(std.ImagePath);
            this.db.SaveChanges();

            return std;
        }

        public void Update(Student student)
        {
            // remove child references
            Student std = this.db.Students.Find(student.StudentId);

            // file
            if (student.ImageFile != null)
            {
                student.ImagePath = Utils.GeneralUtils.UploadToServer(student.ImageFile);
                Utils.GeneralUtils.DeleteFileAtServer(std.ImagePath);
                std.ChangeImage(student);
            }
            else
                student.ChangeImage(std);
   
            foreach (var e in std.Teachers.ToList())
                std.Teachers.Remove(e);

            // update student info content
            std.Update(student, this.db);
            db.Entry(std).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }
    }
}