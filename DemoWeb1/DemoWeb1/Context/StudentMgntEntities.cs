﻿using DemoWeb1.Models;
using System.Data.Entity;

namespace DemoWeb1.Context
{
    public class StudentMgntEntities : DbContext
    {
        public StudentMgntEntities() : base("StudentMgnt")
        {

        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Teacher> Teachers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}