﻿using DemoWeb1.Models;
using DemoWeb1.Service;
using DemoWeb1.Utils;
using DemoWeb1.Utils.Constants;
using System.Web.Mvc;
using System.Web.Security;

namespace DemoWeb1.Controllers
{
    public class LoginController : Controller
    {
        LoginService loginService = new LoginService(new Context.StudentMgntEntities());

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User user)
        {
            User result = (User)loginService.isValidAccount(user.UserName, user.Password);

            if (result != null)
            {
                // storing in cookie
                FormsAuthentication.SetAuthCookie(user.UserName, true);

				// redirect by role
                if (GeneralUtils.IsStudent(result))
                    return RedirectToAction(RepoConstants.ACTION_INDEX, RepoConstants.CONTROLLER_STUDENT);
                else
                    return RedirectToAction(RepoConstants.ACTION_INDEX, RepoConstants.CONTROLLER_TEACHER);
            }
            else
            {
                ModelState.AddModelError(string.Empty, RepoConstants.USERNAME_PASSWORD_INVALID);
            }
            return View();
        }

        public ActionResult Signup()
        {
            return RedirectToAction(RepoConstants.ACTION_CREATE, RepoConstants.CONTROLLER_STUDENT);
        }
    }
}