﻿using DemoWeb1.Context;
using DemoWeb1.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using DemoWeb1.Service;
using DemoWeb1.Utils.Constants;
using System.Collections.Generic;

namespace DemoWeb1.Controllers
{
    public class StudentController : Controller
    {
        StudentMgntEntities db = new StudentMgntEntities();
        
        // GET: Student
        public ActionResult Index()
        {
            // TODO: Add insert logic here
            return View(db.Students.ToList());
        }

        public ActionResult Search(string SearchString)
        {
            if (String.IsNullOrEmpty(SearchString))
                return RedirectToAction(RepoConstants.ACTION_INDEX);

            StudentService service = new StudentService(db);
            return View(RepoConstants.ACTION_INDEX, service.FindStudentByString(SearchString));
        }

        [HttpPost]
        public ActionResult SearchAutocomplete(string SearchString)
        {
            StudentService service = new StudentService(db);
            List<String> listAutocomplete = service.SearchAutocomplete(SearchString);

            return Json(listAutocomplete.Take(RepoConstants.AUTOCOMPLETE_ITEM_NUMBER), JsonRequestBehavior.AllowGet);
        }

        // GET: Student/Details/5
        public ActionResult Details(int id)
        {
            Student std = db.Students.Find(id);
            var result = (std.Teachers.Select(teacher => teacher.TeacherId)).ToList();
            std.TeacherListId = result.ToList();

            return View(std);
        }

        // GET: Student/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Student/Create
        [HttpPost]
        public ActionResult Create(Student student)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    StudentService service = new StudentService(db);
                    service.Create(student);
                }

                return RedirectToAction(RepoConstants.ACTION_INDEX);
            }
            catch
            {
                return View();
            }
        }

        // GET: Student/Edit/5
        public ActionResult Edit(int id)
        {
            Student std = db.Students.Find(id);
            var result = (std.Teachers.Select(teacher => teacher.TeacherId)).ToList();
            std.TeacherListId = result.ToList();

            return View(std);
        }

        // POST: Student/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Student student)
        {
            try
            {
                StudentService service = new StudentService(db);
                service.Update(student);

                return RedirectToAction(RepoConstants.ACTION_INDEX);
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Diagnostics.Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}",
                            validationErrors.Entry.Entity.GetType().FullName,
                            validationError.PropertyName,
                            validationError.ErrorMessage);
                    }
                }

                return View();
            }
        }

        // GET: Student/Delete/5
        public ActionResult Delete(int id)
        {
            // deleting
            StudentService service = new StudentService(db);
            Student std = service.Delete(id);

            return RedirectToAction(RepoConstants.ACTION_INDEX);
        }

        // POST: Student/Delete/5
        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                // deleting
                StudentService service = new StudentService(db);
                Student std = service.Delete(id);

                return RedirectToAction(RepoConstants.ACTION_INDEX);
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Diagnostics.Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}",
                            validationErrors.Entry.Entity.GetType().FullName,
                            validationError.PropertyName,
                            validationError.ErrorMessage);
                    }
                }

                return View();
            }
        }
    }
}
