﻿using System.Web.Mvc;

namespace DemoWeb1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}