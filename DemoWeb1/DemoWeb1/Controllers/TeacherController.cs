﻿using DemoWeb1.Context;
using DemoWeb1.Models;
using DemoWeb1.Utils.Constants;
using System.Linq;
using System.Web.Mvc;

namespace DemoWeb1.Controllers
{
    public class TeacherController : Controller
    {
        StudentMgntEntities db = new StudentMgntEntities();

        // GET: Teacher
        public ActionResult Index()
        {
            return View(db.Teachers.ToList());
        }

        // GET: Teacher/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Teacher/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: Teacher/Create
        [HttpPost]
        public ActionResult Create(Teacher teacher)
        {
            try
            {
                if (this.db.Teachers.ToList().Count() == 0)
                    return RedirectToAction(RepoConstants.CONTROLLER_STUDENT + RepoConstants.SLASH + RepoConstants.ACTION_INDEX);
                else
                    return RedirectToAction(RepoConstants.ACTION_INDEX);
            }
            catch
            {
                return View();
            }
        }

        // GET: Teacher/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Teacher/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                return RedirectToAction(RepoConstants.ACTION_INDEX);
            }
            catch
            {
                return View();
            }
        }

        // GET: Teacher/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Teacher/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(RepoConstants.ACTION_INDEX);
            }
            catch
            {
                return View();
            }
        }
    }
}
