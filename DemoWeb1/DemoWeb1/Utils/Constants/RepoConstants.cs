﻿using System;

namespace DemoWeb1.Utils.Constants
{
    public class RepoConstants
    {
        /**
         * ACTION and CONTROLLER Constant Values
         */
        public const String ACTION_INDEX = "Index";
        public const String ACTION_CREATE = "Create";
        public const String CONTROLLER_STUDENT = "Student";
        public const String CONTROLLER_TEACHER = "Teacher";

        /**
         * Constant Values performing INVALID MESSAGES 
         */
        public const String USERNAME_PASSWORD_INVALID = "Username or password is invalid";

        /**
         * UTILS Constant Values
         */
        public const String INDEX_FIRST_SECOND = "IX_FirstAndSecond";
        public const String SLASH = "/";
        public const String HYPHEN = "-";
        public const String DATE_FORMAT_PATTERN = "yymmssfff";
        public const String IMAGE_FOLDER_PATH = "~/Image/";
        public const int ROLE_STUDENT = 1;
        public const int ROLE_TEACHER = 2;
        public const int AUTOCOMPLETE_ITEM_NUMBER = 6;

        /**
         * NAME's constant properties
         */
        public const int NAME_SIZE = 50;
        public const String NAME_SIZE_ERROR = "Name cannot be longer than 50 characters";
        //public readonly String NAME_SIZE_ERROR { get => NAME_SIZE_MESSAGE; set => NAME_SIZE_MESSAGE = value; }
        public const String NAME_COLUMN = "Name";

        /**
         * USERNAME's constant properties
         */
        public const int USERNAME_SIZE = 100;
        public static String USERNAME_SIZE_ERROR = String.Empty;
        public const String USERNAME_COLUMN = "User name";

        /**
         * PASSWORD's constant properties
         */
        public const String PASSWORD_COLUMN = "Password";
        public const String CONFIRMPASSWORD_MSG = "Please confirm your password";

        /**
         * ROLE's constant properties
         */
        public const String ROLE_COLUMN = "Role";

        /**
         * IMAGE PROFILE's constant properties
         */
        public const String IMG_DISPLAY_NAME = "Upload File";
        public const String IMG_COLUMN = "Image";
    }
}