﻿using DemoWeb1.Models;
using DemoWeb1.Utils.Constants;
using System;
using System.IO;
using System.Web;

namespace DemoWeb1.Utils
{
    public class GeneralUtils
    {
        public static string Encode(string value)
        {
            var hash = System.Security.Cryptography.SHA1.Create();
            var encoder = new System.Text.ASCIIEncoding();
            var combined = encoder.GetBytes(value ?? String.Empty);
            return BitConverter.ToString(hash.ComputeHash(combined)).ToLower().Replace(RepoConstants.HYPHEN, String.Empty);
        }

        public static String UploadToServer(HttpPostedFileBase file)
        {
            // move picture to project place (folder Image)
            String filename = Path.GetFileNameWithoutExtension(file.FileName);
            String extension = Path.GetExtension(file.FileName);
            filename = filename + DateTime.Now.ToString(RepoConstants.DATE_FORMAT_PATTERN) + extension;
            String ImagePath = RepoConstants.IMAGE_FOLDER_PATH + filename;
            filename = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(RepoConstants.IMAGE_FOLDER_PATH), filename);
            file.SaveAs(filename);

            return ImagePath;
        }

        public static void DeleteFileAtServer(String filename)
        {
            if (!String.IsNullOrEmpty(filename))
                System.IO.File.Delete(System.Web.Hosting.HostingEnvironment.MapPath(filename));
        }
		
		public static bool IsStudent(User user) {
			return user is Student;
		}
		
		public static bool IsTeacher(User user) {
			return user is Teacher;
		}
		
		public int IdentifyUserType(User user){
			int result = 0;
			
			if (GeneralUtils.IsStudent(user))
				result = RepoConstants.ROLE_STUDENT;
			else if (GeneralUtils.IsTeacher(user))
				result = RepoConstants.ROLE_TEACHER;
			
			return result;
		}
    }
}