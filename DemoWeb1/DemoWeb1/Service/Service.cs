﻿using DemoWeb1.Context;

namespace DemoWeb1.Service
{
    public abstract class Service
    {
        protected StudentMgntEntities db;
        protected QueryEntitiesDAO entitiesDAO;

        public Service(StudentMgntEntities dbMgnt)
        {
            this.db = dbMgnt;
            entitiesDAO = new QueryEntitiesDAO(dbMgnt);
        }
    }
}