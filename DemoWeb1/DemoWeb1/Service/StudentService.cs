﻿using DemoWeb1.Context;
using DemoWeb1.Models;
using System;
using System.Collections.Generic;

namespace DemoWeb1.Service
{
    public interface IStudentService
    {
        void Create(Student student);
        Student Delete(int id);
        void Update(Student student);
        List<Student> FindStudentByString(String SearchString);
        List<String> SearchAutocomplete(String SrchString);
    }

    public class StudentService : Service, IStudentService
    {
        private StudentDAO studentDAO;

        public StudentService(StudentMgntEntities dbMgnt) : base(dbMgnt)
        {
            studentDAO = new StudentDAO(dbMgnt);
        }

        public void Create(Student student)
        {
            studentDAO.Create(student);
        }

        public Student Delete(int id)
        {
            return studentDAO.Delete(id);
        }

        public List<Student> FindStudentByString(string SearchString)
        {
            return this.entitiesDAO.FindStudentByString(SearchString);
        }

        public List<string> SearchAutocomplete(string SrchString)
        {
            return this.entitiesDAO.SearchAutocomplete(SrchString);
        }

        public void Update(Student student)
        {
            this.studentDAO.Update(student);
        }
    }
}