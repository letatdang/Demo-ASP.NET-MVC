﻿using DemoWeb1.Context;
using System;

namespace DemoWeb1.Service
{
    public interface ILoginService
    {
        Object isValidAccount(String _username, String _password);
    }

    public class LoginService : Service, ILoginService
    {
        public LoginService(StudentMgntEntities dbMgnt) : base(dbMgnt)
        {
        }

        public Object isValidAccount(String _username, String _password)
        {
            return this.entitiesDAO.isValidAccount(_username, _password);
        }
    }
}